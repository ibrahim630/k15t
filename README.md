Registration project is a very tiny register page for Java Meetup.

##Instructions to build and run the project##
-----------------------------------------
- Checkout the project on https://ibrahim630@bitbucket.org/ibrahim630/k15t
- To build project , type " mvnw package "
- To run the project type " mvnw spring-boot:run "

After running the project you can go to http://localhost:8080 from any browser, it will redirect you to http://localhost:8080/registration.html

You can fill the registration informations. Phone number is not required. When the validation is ok green arrow will see in the
form. 

##The technologies used for project##
-----------------------------------------
- Bootstrap, Jquery, Velocity Template Engine
- Spring Boot 1.3.6.RELEASE, Spring Data JPA, Jersey RestApi, Spring TestRestTemplate