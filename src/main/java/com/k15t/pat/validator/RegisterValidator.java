package com.k15t.pat.validator;

import com.k15t.pat.model.RegisterData;
import com.k15t.pat.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RegisterValidator implements Validator {
    @Autowired
    private RegisterService registerService;

    @Override
    public boolean supports(Class<?> aClass) {
        return RegisterData.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        RegisterData registerData = (RegisterData) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "This field is required.");
        if (registerData.getName().length() < 6 || registerData.getName().length() > 32) {
            errors.rejectValue("name", "Please use between 6 and 32 characters.");
        }
        if (registerService.findByName(registerData.getName()) != null) {
            errors.rejectValue("name", "Someone already has that username.");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "This field is required.");
        if (registerData.getPassword().length() < 8 || registerData.getPassword().length() > 32) {
            errors.rejectValue("password", "Try one with at least 8 characters in password.");
        }

        if (!registerData.getPasswordConfirm().equals(registerData.getPassword())) {
            errors.rejectValue("passwordConfirm", "These passwords don't match.");
        }

    }
}