package com.k15t.pat.repository;

import com.k15t.pat.model.RegisterData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegisterRepository extends JpaRepository<RegisterData,Long> {
    RegisterData findByName(String name);
}
