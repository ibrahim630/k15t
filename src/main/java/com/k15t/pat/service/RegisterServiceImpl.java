package com.k15t.pat.service;

import com.k15t.pat.model.RegisterData;
import com.k15t.pat.repository.RegisterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegisterServiceImpl implements RegisterService{
    @Autowired
    private RegisterRepository registerRepository;

    @Override
    public void save(RegisterData registerData) {
        registerRepository.save(registerData);
    }

    @Override
    public RegisterData findByName(String name) {
        return registerRepository.findByName(name);
    }
}
