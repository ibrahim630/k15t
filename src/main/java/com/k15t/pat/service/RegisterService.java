package com.k15t.pat.service;

import com.k15t.pat.model.RegisterData;

public interface RegisterService {
    void save(RegisterData registerData);
    RegisterData findByName(String name);
}
