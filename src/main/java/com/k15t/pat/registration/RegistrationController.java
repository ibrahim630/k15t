package com.k15t.pat.registration;

import com.k15t.pat.model.RegisterData;
import com.k15t.pat.validator.RegisterValidator;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.StringWriter;
import java.util.Map;


@RestController
public class RegistrationController {
    @Autowired
    private VelocityEngine velocityEngine;

    @Autowired
    RegistrationResource registrationResource;

    @Autowired
    RegisterValidator registerValidator;

    @RequestMapping("/registration.html")
    public String registration() {

        Template template = velocityEngine.getTemplate("templates/registration.vm");
        VelocityContext context = new VelocityContext();
        StringWriter writer = new StringWriter();
        template.merge(context, writer);

        return writer.toString();
    }


    @RequestMapping(value = "/registerMeetup")
    public ModelAndView addRegister(@ModelAttribute("registerForm") RegisterData registerForm, BindingResult bindingResult, Map<String, Object> model) {

        registerValidator.validate(registerForm, bindingResult);

        if (bindingResult.hasErrors()) {
            model.put("status", "fail");
            model.put("errMessage",bindingResult.getAllErrors());
            return new ModelAndView("registration", model);
        }


        WebTarget target = ClientBuilder.newClient().target("http://localhost:8080/rest/registration/save");
        Response response = target.request().post(Entity.json(registerForm));

        if (response.getStatus() == 200) {
            model.put("status", "success");
        }

        return new ModelAndView("registration", model);
    }


}

