package com.k15t.pat.registration;

import com.k15t.pat.model.RegisterData;
import com.k15t.pat.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/registration")
@Component
public class RegistrationResource {

    @Autowired
    private RegisterService registerService;

    @Path("/save")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response save(RegisterData registerData) {
        registerService.save(registerData);
        return Response.ok().build();
    }

}

