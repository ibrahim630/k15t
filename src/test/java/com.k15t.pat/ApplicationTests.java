package com.k15t.pat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.k15t.pat.model.RegisterData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationBootstrap.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ApplicationTests {

    @Value("${local.server.port}")
    private int port;
    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    public void shouldSaveToDatabase() {

        RegisterData registerData = new RegisterData();
        registerData.setName("Hussmand");
        registerData.setPassword("Zz1234567.!");
        registerData.setAddress("Haydar Aliyev Sok.");
        registerData.setEmail("ibrahimavci@outlook.com");
        registerData.setPhoneNumber("254526485");

        HttpEntity<RegisterData> entity = new HttpEntity<>(registerData, headers);

        ResponseEntity<Response> response = restTemplate.exchange(
                createURLWithPort("/rest/registration/save"),
                HttpMethod.POST, entity, Response.class);

        assertEquals("/rest/registration/save Should return status 200", 200, response.getStatusCode().value());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

}
